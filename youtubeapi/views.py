from django.shortcuts import render
from rest_framework import viewsets, status, permissions
from rest_framework.response import Response
from .models import Video, Rating
from .serializers import VideoSerializer, RatingSerializer

# Create your views here.

class VideoViewSet(viewsets.ModelViewSet):
    """
    Viewset for video model.
    """
    
    queryset = Video.objects.all()
    serializer_class = VideoSerializer
    permission_classes = (permissions.AllowAny, )


class RatingViewSet(viewsets.ModelViewSet):
    """
    Viewset for rating model.
    """
    
    queryset = Rating.objects.all()
    serializer_class = RatingSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def delete(self, request, *args, **kwargs):
        response = {
            'message': 'Rating cannot be updated like this'
        }
        return Response(response)
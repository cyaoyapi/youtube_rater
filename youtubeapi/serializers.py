from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from .models import Video, Rating

# Create your serializers here.

class VideoSerializer(serializers.ModelSerializer):
    """
    Serializer for Video model.
    """

    class Meta:
        model = Video
        fields = '__all__'
        extras_kwargs = {
            'url': {
                'required': True
            }
        }


class RatingSerializer(serializers.ModelSerializer):
    """
    Serializer for Rating model.
    """

    class Meta:
        model = Rating
        fields = ('id', 'video', 'user', 'stars', 'comments')


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for User model.
    """

    class Meta:
        model = User
        fields = ('id', 'username', 'password')
        extras_kwargs = {
            'password': {
                'required': True,
                'write_only': True
            }
        }

        def create(self, validated_data):
            """
            Create a token for a user when we create it.
            """

            user = User.objects.create_user(**validated_data)
            Token.objects.create(user=user)
            return user
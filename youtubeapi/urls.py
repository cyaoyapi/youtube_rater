from django.urls import path, include
from rest_framework import routers

router = routers.DefaultRouter()
#router.register(r'videos', VideoViewSet)
urlpatterns = [
    path('', include(router.urls)),
]
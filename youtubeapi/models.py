from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Video(models.Model):
    """
    A model to create a video.
    """

    title = models.CharField(max_length=80)
    description = models.TextField(max_length=300)
    title = models.CharField(max_length=80)
    url = models.URLField()
    category = models.CharField(max_length=50)
    author = models.TextField(max_length=50)


class Rating(models.Model):
    """
    A model to create a rating on a video.
    """

    video = models.ForeignKey(Video, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    stars = models.IntegerField()
    comments = models.TextField(max_length=300)

    class Meta:
        unique_together = (('video', 'user'),)
        index_together = (('video', 'user'),)
